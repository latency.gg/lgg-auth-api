import * as db from "@latency.gg/lgg-auth-db";
import * as authApi from "@latency.gg/lgg-auth-oas";
import * as http from "http";
import { minute, second } from "msecs";
import pg from "pg";
import * as prom from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

interface Context {
    endpoints: {
        application: URL,
        pg: URL,
    },
    servers: {
        application: application.Server,
    },
    services: application.Services,
    createApplicationClient(credentials?: authApi.ClientCredentials): authApi.Client,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
) {
    const result = withTestDb(
        db.getSchema(),
        new URL(process.env.PGURI || "postgres://postgres@localhost:5432/postgres"),
        async pgUri => {
            const endpoints = {
                application: new URL("http://localhost:8080"),
                pg: pgUri,
            };

            const abortController = new AbortController();
            const promRegistry = new prom.Registry();

            const applicationConfig: application.Config = {
                endpoint: endpoints.application,
                pgUri: endpoints.pg,

                accessTokenAge: 10 * second,
                refreshTokenAge: 1 * minute,
                authorizationCodeAge: 5 * second,

                linger: 1 * minute,
                retryIntervalBase: 1 * second,
                retryIntervalCap: 1 * second,

                connectionTimeout: 10 * second,
                connectionLinger: 1 * minute,
                queryTimeout: 10 * second,

                abortController,
                promRegistry,
            };

            const onError = (error: unknown) => {
                if (!abortController.signal.aborted) {
                    throw error;
                }
            };

            const applicationContext = application.createContext(
                applicationConfig,
                onError,
            );
            try {

                const servers = {
                    application: application.createServer(
                        applicationContext,
                        onError,
                    ),
                };

                const httpServers = {
                    application: http.createServer(servers.application.asRequestListener({
                        onError,
                    })),
                };

                const context = {
                    endpoints,
                    servers,
                    createApplicationClient(credentials?: authApi.ClientCredentials) {
                        return new authApi.Client(
                            {
                                baseUrl: endpoints.application,
                            },
                            credentials,
                        );
                    },
                    services: applicationContext.services,
                };

                const keys = Object.keys(httpServers) as Array<keyof typeof httpServers>;
                await Promise.all(
                    keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
                        endpoints[key].port,
                        () => resolve(),
                    ))),
                );

                try {
                    await job(context);
                }
                finally {
                    abortController.abort();

                    await Promise.all(
                        keys.map(async key => new Promise<void>(
                            (resolve, reject) => httpServers[key].close(
                                error => error ?
                                    reject(error) :
                                    resolve(),
                            )),
                        ),
                    );
                }
            }
            finally {
                await applicationContext.destroy();
            }

        },
    );

    return result;
}

async function withTestDb<T>(
    sql: string,
    pgAdminUri: URL,
    job: (pgUri: URL) => Promisable<T>,
) {
    const dbName = `testdb-${new Date().valueOf()}`;
    const pgUri = new URL(`/${dbName}`, pgAdminUri);

    const pgAdminPool = new pg.Pool({
        connectionString: pgAdminUri.toString(),
    });
    try {
        await pgAdminPool.query(`CREATE DATABASE "${dbName}";`);
        try {
            const pgPool = new pg.Pool({
                connectionString: pgUri.toString(),
            });
            try {
                await pgPool.query(sql);

                const result = await job(pgUri);
                return result;
            }
            finally {
                await pgPool.end();
            }
        }
        finally {
            await pgAdminPool.query(`DROP DATABASE "${dbName}";`);
        }
    }
    finally {
        await pgAdminPool.end();
    }

}

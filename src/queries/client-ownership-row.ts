import * as authDb from "@latency.gg/lgg-auth-db";
import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { convertPgBinaryToBuffer, createRowQueryFactory, QuerySource, RowQueryState } from "../utils/index.js";

//#region query

export type ClientOwnershipRowQuery = InstanceMemoizer<
    Promise<ClientOwnershipRowQuerySource>, [Partial<authDb.ClientOwnershipRow>]
>

export type ClientOwnershipRowQuerySource = QuerySource<
    ClientOwnershipRowState, authDb.ClientOwnershipRowEvent
>

export function createClientOwnershipRowQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "pgPool">,
    onError: (error: unknown) => void,
): ClientOwnershipRowQuery {
    return createRowQueryFactory({
        pool: services.pgPool,
        getPrimaryKey: key => makePrimaryKey(key),
        getIndexKey: {},
        onError,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
        createTableEvents: authDb.createClientOwnershipRowEvents,
    });

}

//#endregion

//#region state / events

export type ClientOwnershipRowState = RowQueryState<authDb.ClientOwnershipRow>;

//#endregion

//#region selectors

export function selectClientOwnershipRow(
    state: ClientOwnershipRowState,
    user: string,
    client: string,
): authDb.ClientOwnershipRow | undefined {
    const key = makePrimaryKey({ user_id: user, client_id: client });
    const row = state.rows.get(key);
    if (!row) return;

    return row;
}

export function createClientOwnershipSnapshotEventFromRowState(
    state: ClientOwnershipRowState,
): authSpec.ClientOwnershipSnapshotSchema {
    const payload = {
        clientOwnerships: state.rows.
            map(row => mapRowToEntity(row)).
            valueSeq().
            toArray(),
    };

    return {
        type: "client-ownership-snapshot",
        payload,
    };
}

export function createClientOwnershipCreatedEventFromRowState(
    state: ClientOwnershipRowState,
    user: string,
    client: string,
): authSpec.ClientOwnershipCreatedSchema {
    const row = selectClientOwnershipRow(state, user, client);
    assert(row != null, "expected row");

    const payload = {
        clientOwnership: mapRowToEntity(row),
    };

    return {
        type: "client-ownership-created",
        payload,
    };
}

export function createClientOwnershipDeletedEventFromRowState(
    state: ClientOwnershipRowState,
    user: string,
    client: string,
): authSpec.ClientOwnershipDeletedSchema {
    const row = selectClientOwnershipRow(state, user, client);
    assert(row != null, "expected row");

    const payload = {
        clientOwnership: mapRowToEntity(row),
    };

    return {
        type: "client-ownership-deleted",
        payload,
    };
}

//#endregion

//#region helpers

function makePrimaryKey(key: authDb.ClientOwnershipRowKey) {
    return [key.user_id, key.client_id].join("\x1f");
}

function mapRowToEntity(
    row: authDb.ClientOwnershipRow,
): authSpec.ClientOwnershipEntitySchema {
    const client = convertPgBinaryToBuffer(row.client_id).toString("base64");
    const created = new Date(row.created_utc).toISOString();

    return {
        client,
        created,
    };
}

//#endregion

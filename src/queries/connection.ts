import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import deepEqual from "fast-deep-equal";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";
import * as connectionRow from "./connection-row.js";

//#region query

export type ConnectionQuery = InstanceMemoizer<
    Promise<ConnectionQuerySource>, [string]
>

export type ConnectionQuerySource = QuerySource<
    ConnectionState, ConnectionEventUnion
>

export function createConnectionQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "connectionRowQuery">,
    onError: (error: unknown) => void,
): ConnectionQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: provider => provider,
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        provider: string,
    ) {
        const queryPromise = services.connectionRowQuery.acquire({
            provider,
        });
        try {
            /**
             * wait for the query has sent it's first event, this will resolve immediately if
             * this query was already setup by an earlier require
             */
            const query = await queryPromise;

            let lastState = query.getState();

            /*
            Always emit a snaphot first
            */
            yield connectionRow.createConnectionSnapshotEventFromRowState(lastState);

            /*
            then the rest of the events
            */
            for await (const { state, event } of query.fork(signal)) {
                if (isErrorEvent(event)) {
                    break;
                }

                switch (event.type) {
                    case "snapshot": {
                        const nextEvent = connectionRow.createConnectionSnapshotEventFromRowState(
                            state,
                        );
                        const prevEvent = connectionRow.createConnectionSnapshotEventFromRowState(
                            lastState,
                        );

                        if (!deepEqual(nextEvent, prevEvent)) {
                            yield nextEvent;
                        }

                        break;
                    }

                    case "insert": {
                        const {
                            provider,
                            subject,
                            user_id: user,
                        } = event.key;
                        const nextEvent = connectionRow.createConnectionCreatedEventFromRowState(
                            state, provider, subject, user,
                        );

                        yield nextEvent;
                        break;
                    }

                    case "delete": {
                        const {
                            provider,
                            subject,
                            user_id: user,
                        } = event.key;
                        const nextEvent = connectionRow.createConnectionDeletedEventFromRowState(
                            lastState, provider, subject, user,
                        );

                        yield nextEvent;
                        break;
                    }

                    default: throw new Error("unexpected row event type");
                }

                lastState = state;
            }
        }
        finally {
            services.connectionRowQuery.release(queryPromise);
        }
    }
}

//#endregion

//#region state / events

export interface ConnectionEntity {
    user: string
    created: string
}

export interface ConnectionState {
    error: boolean;
    entities: immutable.Map<string, ConnectionEntity>;
}

const initialState: ConnectionState = {
    error: false,
    entities: immutable.Map<string, ConnectionEntity>(),
};

export type ConnectionEventUnion =
    authSpec.ConnectionSnapshotSchema |
    authSpec.ConnectionCreatedSchema |
    authSpec.ConnectionDeletedSchema;

function reduce(
    state: ConnectionState,
    event: ErrorEvent | ConnectionEventUnion,
): ConnectionState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "connection-snapshot": {
            let { entities } = initialState;
            entities = entities.asMutable();

            for (
                const {
                    subject,
                    user,
                    created,
                } of event.payload.connections
            ) {
                assert(!entities.has(subject), "connection already exist");

                const entity = {
                    user,
                    created,
                };
                entities.set(subject, entity);
            }

            entities.asImmutable();

            return {
                error: false,
                entities,
            };
        }

        case "connection-created": {
            let { entities } = state;

            const {
                subject,
                user,
                created,
            } = event.payload.connection;

            assert(!entities.has(subject), "connection already exist");

            const entity: ConnectionEntity = {
                user,
                created,
            };
            entities = entities.set(subject, entity);

            return {
                ...state,
                entities,
            };
        }

        case "connection-deleted": {
            let { entities } = state;

            const { subject } = event.payload.connection;

            assert(entities.has(subject), "connection does not exist");
            entities = entities.delete(subject);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectConnectionSnapshotEvent(
    state: ConnectionState,
): authSpec.ConnectionSnapshotSchema {
    const payload = {
        connections: state.entities.
            map(({ user, created }, subject) => ({
                subject,
                user,
                created,
            })).
            valueSeq().
            toArray(),
    };

    return {
        type: "connection-snapshot",
        payload,
    };
}

export function selectConnectionExists(
    state: ConnectionState,
    connection: string,
): boolean {
    return state.entities.has(connection);
}

export function selectConnectionEntity(
    state: ConnectionState,
    connection: string,
): ConnectionEntity | undefined {
    const connections = state.entities;
    const entity = connections.get(connection);
    if (!entity) return;

    return entity;
}

//#endregion

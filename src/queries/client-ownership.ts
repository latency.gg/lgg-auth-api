import * as authSpec from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import deepEqual from "fast-deep-equal";
import immutable from "immutable";
import { InstanceMemoizer } from "instance-memoizer";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer, createQueryFactory, ErrorEvent, isErrorEvent, QuerySource } from "../utils/index.js";
import * as clientOwnershipRow from "./client-ownership-row.js";

//#region query

export type ClientOwnershipQuery = InstanceMemoizer<
    Promise<ClientOwnershipQuerySource>, [string]
>

export type ClientOwnershipQuerySource = QuerySource<
    ClientOwnershipState, ClientOwnershipEventUnion
>

export function createClientOwnershipQueryFactory(
    config: application.Config,
    services: Pick<application.Services, "clientOwnershipRowQuery">,
    onError: (error: unknown) => void,
): ClientOwnershipQuery {
    return createQueryFactory({
        initialState, reduce,
        createSource,
        calculateHash: user => user,
        onError,
        settle: () => true,
        retryIntervalBase: config.retryIntervalBase,
        retryIntervalCap: config.retryIntervalCap,
    });

    async function* createSource(
        signal: AbortSignal,
        user: string,
    ) {
        const userBuffer = Buffer.from(user, "base64");
        const userPgBinary = convertPgBinaryFromBuffer(userBuffer);
        const queryPromise = services.clientOwnershipRowQuery.acquire({
            user_id: userPgBinary,
        });
        try {
            /**
             * wait for the query has sent it's first event, this will resolve immediately if
             * this query was already setup by an earlier require
             */
            const query = await queryPromise;

            let lastState = query.getState();

            /*
            Always emit a snaphot first
            */
            yield clientOwnershipRow.createClientOwnershipSnapshotEventFromRowState(lastState);

            /*
            then the rest of the events
            */
            for await (const { state, event } of query.fork(signal)) {
                if (isErrorEvent(event)) {
                    break;
                }

                switch (event.type) {
                    case "snapshot": {
                        const nextEvent =
                            clientOwnershipRow.createClientOwnershipSnapshotEventFromRowState(
                                state,
                            );
                        const prevEvent =
                            clientOwnershipRow.createClientOwnershipSnapshotEventFromRowState(
                                lastState,
                            );

                        if (!deepEqual(nextEvent, prevEvent)) {
                            yield nextEvent;
                        }

                        break;
                    }

                    case "insert": {
                        const {
                            user_id: user,
                            client_id: client,
                        } = event.key;
                        const nextEvent =
                            clientOwnershipRow.createClientOwnershipCreatedEventFromRowState(
                                state, user, client,
                            );

                        yield nextEvent;
                        break;
                    }

                    case "delete": {
                        const {
                            user_id: user,
                            client_id: client,
                        } = event.key;
                        const nextEvent =
                            clientOwnershipRow.createClientOwnershipDeletedEventFromRowState(
                                lastState, user, client,
                            );

                        yield nextEvent;
                        break;
                    }

                    default: throw new Error("unexpected row event type");
                }

                lastState = state;
            }
        }
        finally {
            services.clientOwnershipRowQuery.release(queryPromise);
        }
    }
}

//#endregion

//#region state / events

export interface ClientOwnershipEntity {
    created: string
}

export interface ClientOwnershipState {
    error: boolean;
    entities: immutable.Map<string, ClientOwnershipEntity>;
}

const initialState: ClientOwnershipState = {
    error: false,
    entities: immutable.Map<string, ClientOwnershipEntity>(),
};

export type ClientOwnershipEventUnion =
    authSpec.ClientOwnershipSnapshotSchema |
    authSpec.ClientOwnershipCreatedSchema |
    authSpec.ClientOwnershipDeletedSchema;

function reduce(
    state: ClientOwnershipState,
    event: ErrorEvent | ClientOwnershipEventUnion,
): ClientOwnershipState {
    if (isErrorEvent(event)) {
        return {
            ...state,
            error: true,
        };
    }

    switch (event.type) {
        case "client-ownership-snapshot": {
            let { entities } = initialState;
            entities = entities.asMutable();

            for (
                const {
                    client,
                    created,
                } of event.payload.clientOwnerships
            ) {
                assert(!entities.has(client), "client-ownership already exist");

                const entity = {
                    created,
                };
                entities.set(client, entity);
            }

            entities.asImmutable();

            return {
                error: false,
                entities,
            };
        }

        case "client-ownership-created": {
            let { entities } = state;

            const {
                client,
                created,
            } = event.payload.clientOwnership;

            assert(!entities.has(client), "client-ownership already exist");

            const entity: ClientOwnershipEntity = {
                created,
            };
            entities = entities.set(client, entity);

            return {
                ...state,
                entities,
            };
        }

        case "client-ownership-deleted": {
            let { entities } = state;

            const { client } = event.payload.clientOwnership;

            assert(entities.has(client), "client-ownership does not exist");
            entities = entities.delete(client);

            return {
                ...state,
                entities,
            };
        }

        default: return state;
    }
}

//#endregion

//#region selectors

export function selectClientOwnershipSnapshotEvent(
    state: ClientOwnershipState,
): authSpec.ClientOwnershipSnapshotSchema {
    const payload = {
        clientOwnerships: state.entities.
            map(({ created }, client) => ({
                client,
                created,
            })).
            valueSeq().
            toArray(),
    };

    return {
        type: "client-ownership-snapshot",
        payload,
    };
}

export function selectClientOwnershipExists(
    state: ClientOwnershipState,
    digest: string,
): boolean {
    return state.entities.has(digest);
}

export function selectClientOwnershipEntity(
    state: ClientOwnershipState,
    digest: string,
): ClientOwnershipEntity | undefined {
    const clients = state.entities;
    const entity = clients.get(digest);
    if (!entity) return;

    return entity;
}

//#endregion

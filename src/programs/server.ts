import * as metaApi from "@latency.gg/lgg-meta-api";
import { isAbortError, waitForAbort } from "abort-tools";
import { program } from "commander";
import delay from "delay";
import * as http from "http";
import { hour, minute, second, week } from "msecs";
import * as prom from "prom-client";
import { WaitGroup } from "useful-wait-group";
import * as application from "../application/index.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    option(
        "--endpoint <url>",
        "endpoint of the service",
        v => new URL(v),
        new URL("http://localhost:8080"),
    ).
    option("--meta-port <number>", "Listen to meta-port", Number, 9090).
    option(
        "--meta-endpoint <url>",
        "endpoint of the meta-service",
        v => new URL(v),
        new URL("http://localhost:9090"),
    ).
    requiredOption("--pg-uri <url>", "pg uri", v => new URL(v)).

    option("--shutdown-delay <msec>", "", Number, 35 * second).

    option("--linger <msec>", "", Number, 15 * minute).
    option("--retry-interval-base <msec>", "", Number, 1 * second).
    option("--retry-interval-cap <msec>", "", Number, 1 * minute).

    option("--access-token-age <seconds>", "", value => Number(value) * second, 1 * hour).
    option("--refresh-token-age <seconds>", "", value => Number(value) * second, 1 * week).
    option("--authorization-code-age <seconds>", "", value => Number(value) * second, 1 * minute).

    option("--connection-timeout <msec>", "", Number, 10 * second).
    option("--connection-linger <msec>", "", Number, 1 * minute).
    option("--query-timeout <msec>", "", Number, 1 * minute).

    action(programAction);

interface ActionOptions {
    port: number;
    metaPort: number;
    endpoint: URL;
    metaEndpoint: URL;

    pgUri: URL;

    shutdownDelay: number;

    linger: number;
    retryIntervalBase: number;
    retryIntervalCap: number;

    accessTokenAge: number;
    refreshTokenAge: number;
    authorizationCodeAge: number;

    connectionTimeout: number,
    connectionLinger: number,
    queryTimeout: number,

}
async function programAction(actionOptions: ActionOptions) {
    const {
        port,
        endpoint,
        metaPort,
        metaEndpoint,
        pgUri,

        shutdownDelay,

        linger,
        retryIntervalBase,
        retryIntervalCap,

        accessTokenAge,
        refreshTokenAge,
        authorizationCodeAge,

        connectionTimeout,
        connectionLinger,
        queryTimeout,
    } = actionOptions;

    console.log("starting...");

    process.
        on("uncaughtException", error => {
            console.error(error);
            process.exit(1);
        }).
        on("unhandledRejection", error => {
            console.error(error);
            process.exit(1);
        });

    const onWarn = (error: unknown) => {
        console.warn(error);
    };

    const livenessController = new AbortController();
    const readinessController = new AbortController();

    const promRegistry = new prom.Registry();
    prom.collectDefaultMetrics({ register: promRegistry });

    {
        /*
        When we receive a signal, the server should be not ready anymore, so
        we abort the readiness controller
        */
        const onSignal = (signal: NodeJS.Signals) => {
            console.log(signal);
            readinessController.abort();
        };
        process.addListener("SIGINT", onSignal);
        process.addListener("SIGTERM", onSignal);
    }

    const metaConfig: metaApi.Config = {
        endpoint: metaEndpoint,

        livenessController,
        readinessController,

        promRegistry,
    };
    const metaContext = metaApi.createContext(metaConfig);

    const config: application.Config = {
        endpoint,

        pgUri,

        accessTokenAge,
        refreshTokenAge,
        authorizationCodeAge,

        linger,
        retryIntervalBase,
        retryIntervalCap,

        abortController: livenessController,
        promRegistry,

        connectionTimeout,
        connectionLinger,
        queryTimeout,
    };

    const applicationContext = application.createContext(config, onWarn);
    try {

        /*
        We use the waitgroup to wait for requests to finish
        */
        const waitGroup = new WaitGroup();

        const applicationServer = application.createServer(
            applicationContext,
            onWarn,
        );
        applicationServer.registerMiddleware(
            (route, request, next) => waitGroup.with(() => next(request)),
        );

        const metaServer = metaApi.createServer(
            metaContext,
            onWarn,
        );
        metaServer.registerMiddleware(
            (route, request, next) => waitGroup.with(() => next(request)),
        );

        const applicationHttpServer = http.createServer(applicationServer.asRequestListener({
            onError: onWarn,
        }));
        const metaHttpServer = http.createServer(metaServer.asRequestListener({
            onError: onWarn,
        }));

        await new Promise<void>(resolve => applicationHttpServer.listen(
            port,
            () => resolve(),
        ));
        try {
            await new Promise<void>(resolve => metaHttpServer.listen(
                metaPort,
                () => resolve(),
            ));

            try {

                console.log("started");

                try {
                    await waitForAbort(readinessController.signal, "application aborted");
                }
                catch (error) {
                    if (!isAbortError(error)) throw error;
                }

                console.log("stopping...");

                /*
                wait a bit, usually for the load balancer to recognize this service as
                not ready and move traffic to another service.
                TODO: implement heartbeat so this will actually work!
                */
                await delay(shutdownDelay);

                /*
                wait for all requests to finish
                */
                await waitGroup.wait();

                /*
                abort the liveness controller to terminate all streaming responses!
                */
                livenessController.abort();

            }
            finally {
                await new Promise<void>((resolve, reject) => metaHttpServer.close(
                    error => error ?
                        reject(error) :
                        resolve(),
                ));
            }
        }
        finally {
            await new Promise<void>((resolve, reject) => applicationHttpServer.close(
                error => error ?
                    reject(error) :
                    resolve(),
            ));
        }

    }
    finally {
        await applicationContext.destroy();
    }

    console.log("stopped");
}

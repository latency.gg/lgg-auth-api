import * as authApi from "@latency.gg/lgg-auth-oas";
import * as crypto from "crypto";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createCreateClientOperation(
    context: application.Context,
): authApi.CreateClientOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();
        const clientName = incomingEntity.name;
        const clientIdBuffer = crypto.randomBytes(12);
        const clientSaltBuffer = crypto.randomBytes(64);

        await withTransaction(
            context.services.pgPool,
            async pgClient => {

                const result = await pgClient.query(
                    `
insert into public.client(
    id, name, salt, created_utc
)
values (
    $1, $2, $3, $4
)
;
`,
                    [
                        clientIdBuffer,
                        clientName,
                        clientSaltBuffer,
                        now.toISOString(),
                    ],
                );
            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {
                    id: clientIdBuffer.toString("base64"),
                };
            },
        };

    };
}

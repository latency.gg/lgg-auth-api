import * as authApi from "@latency.gg/lgg-auth-oas";
import * as application from "../application/index.js";
import { selectConnectionSnapshotEvent } from "../queries/index.js";
import { mapQueryEvents } from "../utils/index.js";

export function createSubscribeConnectionEventsOperation(
    context: application.Context,
): authApi.SubscribeConnectionEventsOperationHandler {
    return function (incomingRequest) {
        const { provider } = incomingRequest.parameters;

        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                const queryPromise = context.services.connectionQuery.acquire(provider);
                try {
                    const query = await queryPromise;
                    const state = query.getState();
                    yield selectConnectionSnapshotEvent(state);
                    yield* mapQueryEvents(query.fork(signal));
                }
                catch (error) {
                    if (!signal?.aborted) throw error;
                }
                finally {
                    context.services.connectionQuery.release(
                        queryPromise,
                        context.config.linger,
                    );
                }
            },
        };
    };
}

import * as authDb from "@latency.gg/lgg-auth-db";
import * as authOas from "@latency.gg/lgg-auth-oas";
import { queryDelete, UnexpectedRowCountError, withTransaction } from "table-access";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer } from "../utils/index.js";

export function createDeleteClientSecretOperation(
    context: application.Context,
): authOas.DeleteClientSecretOperationHandler {
    return async function (incomingRequest) {
        const { client, digest } = incomingRequest.parameters;

        const clientBuffer = Buffer.from(client, "base64");
        const digestBuffer = Buffer.from(digest, "base64");

        const clientPgBinary = convertPgBinaryFromBuffer(clientBuffer);
        const digestPgBinary = convertPgBinaryFromBuffer(digestBuffer);

        try {
            await withTransaction(context.services.pgPool, async client => {
                await queryDelete(
                    client,
                    authDb.clientSecretRowDescriptor,
                    {
                        client_id: clientPgBinary,
                        digest: digestPgBinary,
                    },
                );
            });
        }
        catch (error) {
            if (error instanceof UnexpectedRowCountError) {
                return {
                    status: 404,
                    parameters: {},
                };
            }

            throw error;
        }

        return {
            status: 204,
            parameters: {},
        };
    };
}

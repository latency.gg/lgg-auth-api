import * as authDb from "@latency.gg/lgg-auth-db";
import * as authApi from "@latency.gg/lgg-auth-oas";
import { queryDelete, UnexpectedRowCountError, withTransaction } from "table-access";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer } from "../utils/index.js";

export function createDeleteClientOperation(
    context: application.Context,
): authApi.DeleteClientOperationHandler {
    return async function (incomingRequest) {
        const { client } = incomingRequest.parameters;
        const clientBuffer = Buffer.from(client, "base64");
        const clientPgBinary = convertPgBinaryFromBuffer(clientBuffer);

        try {
            await withTransaction(
                context.services.pgPool,
                async pgClient => {
                    await queryDelete(
                        pgClient,
                        authDb.clientRowDescriptor,
                        {
                            id: clientPgBinary,
                        },
                    );
                },
            );
        }
        catch (error) {
            if (error instanceof UnexpectedRowCountError) {
                return {
                    status: 404,
                    parameters: {},
                };
            }

            throw error;
        }

        return {
            status: 204,
            parameters: {},
        };

    };
}


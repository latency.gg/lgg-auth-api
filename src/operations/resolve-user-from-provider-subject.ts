import * as authApi from "@latency.gg/lgg-auth-oas";
import assert from "assert";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createResolveUserFromProviderSubjectOperation(
    context: application.Context,
): authApi.ResolveUserFromProviderSubjectOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const provider = incomingRequest.parameters.provider;
        const subject = incomingRequest.parameters.subject;

        const userIdBuffer = await withTransaction(
            context.services.pgPool,
            async pgClient => {
                const result = await pgClient.query(
                    `
select user_id
from public.connection
where provider = $1
and subject = $2
;
`,
                    [
                        provider,
                        subject,
                    ],
                );

                assert(result.rowCount === 1);

                const [connectionRow] = result.rows;

                const userIdBuffer = connectionRow.user_id as Buffer;
                return userIdBuffer;
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    user: userIdBuffer.toString("base64"),
                };
            },
        };
    };
}

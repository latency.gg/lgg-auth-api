import * as authApi from "@latency.gg/lgg-auth-oas";
import * as application from "../application/index.js";
import { selectClientOwnershipSnapshotEvent } from "../queries/index.js";
import { mapQueryEvents } from "../utils/index.js";

export function createSubscribeClientOwnershipEventsOperation(
    context: application.Context,
): authApi.SubscribeClientOwnershipEventsOperationHandler {
    return function (incomingRequest) {
        const { user } = incomingRequest.parameters;

        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                const queryPromise = context.services.clientOwnershipQuery.acquire(user);
                try {
                    const query = await queryPromise;
                    const state = query.getState();
                    yield selectClientOwnershipSnapshotEvent(state);
                    yield* mapQueryEvents(query.fork(signal));
                }
                catch (error) {
                    if (!signal?.aborted) throw error;
                }
                finally {
                    context.services.clientOwnershipQuery.release(
                        queryPromise,
                        context.config.linger,
                    );
                }
            },
        };
    };
}

import * as authDb from "@latency.gg/lgg-auth-db";
import assert from "assert";
import * as pg from "pg";
import { queryInsert, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

test("client-ownership crud flow", t => withContext(async context => {
    await withTransaction(context.services.pgPool, initializeMocks);

    const applicationClient = context.createApplicationClient();

    const user = "qg==";
    const client = "uw==";
    const subscribeClientOwnershipResult = await applicationClient.subscribeClientOwnershipEvents({
        parameters: {
            user,
        },
    });
    assert(subscribeClientOwnershipResult.status === 200);

    const abortController = new AbortController();
    const eventIterable = subscribeClientOwnershipResult.entities(abortController.signal);
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-ownership-snapshot");
            t.deepEqual(
                eventNext.value.payload.clientOwnerships,
                [
                ],
            );
        }

        let created: string;
        {
            const result = await applicationClient.createClientOwnership({
                parameters: {
                    user,
                    client,
                },
                entity() {
                    return {};
                },
            });
            assert(result.status === 201);
            const entity = await result.entity();

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-ownership-created");

            created = eventNext.value.payload.clientOwnership.created;
            t.deepEqual(
                eventNext.value.payload.clientOwnership,
                {
                    client,
                    created,
                },
            );
        }

        {
            const result = await applicationClient.deleteClientOwnership({
                parameters: {
                    user,
                    client,
                },
            });
            assert(result.status === 204);

            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type === "client-ownership-deleted");
            t.deepEqual(
                eventNext.value.payload.clientOwnership,
                {
                    client,
                    created,
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();
        await eventIterator.next().catch(error => t.pass());
    }

    async function initializeMocks(client: pg.ClientBase) {
        await queryInsert(
            client,
            authDb.userRowDescriptor,
            {
                id: "\\xaa",
                name: "test-user",
                salt: "\\x00",
                created_utc: "2022-04-12T14:52:00",
            },
        );

        await queryInsert(
            client,
            authDb.clientRowDescriptor,
            {
                id: "\\xbb",
                name: "test-client",
                salt: "\\x00",
                created_utc: "2022-04-12T14:53:30",
            },
        );

    }
}));

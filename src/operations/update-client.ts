import * as authDb from "@latency.gg/lgg-auth-db";
import * as authApi from "@latency.gg/lgg-auth-oas";
import { queryUpdate, UnexpectedRowCountError, withTransaction } from "table-access";
import * as application from "../application/index.js";
import { convertPgBinaryFromBuffer } from "../utils/index.js";

export function createUpdateClientOperation(
    context: application.Context,
): authApi.UpdateClientOperationHandler {
    return async function (incomingRequest) {
        const { client } = incomingRequest.parameters;
        const clientBuffer = Buffer.from(client, "base64");
        const clientPgBinary = convertPgBinaryFromBuffer(clientBuffer);
        const entity = await incomingRequest.entity();

        try {
            await withTransaction(
                context.services.pgPool,
                async pgClient => {
                    await queryUpdate(
                        pgClient,
                        authDb.clientRowDescriptor,
                        {
                            id: clientPgBinary,
                        },
                        {
                            name: entity.name,
                        },
                    );
                },
            );
        }
        catch (error) {
            if (error instanceof UnexpectedRowCountError) {
                return {
                    status: 404,
                    parameters: {},
                };
            }

            throw error;
        }

        return {
            status: 204,
            parameters: {},
        };

    };
}


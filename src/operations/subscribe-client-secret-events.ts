import * as authApi from "@latency.gg/lgg-auth-oas";
import * as application from "../application/index.js";
import { selectClientSecretSnapshotEvent } from "../queries/index.js";
import { mapQueryEvents } from "../utils/index.js";

export function createSubscribeClientSecretEventsOperation(
    context: application.Context,
): authApi.SubscribeClientSecretEventsOperationHandler {
    return function (incomingRequest) {
        const { client } = incomingRequest.parameters;

        return {
            status: 200,
            parameters: {},
            async * entities(signal) {
                const queryPromise = context.services.clientSecretQuery.acquire(client);
                try {
                    const query = await queryPromise;
                    const state = query.getState();
                    yield selectClientSecretSnapshotEvent(state);
                    yield* mapQueryEvents(query.fork(signal));
                }
                catch (error) {
                    if (!signal?.aborted) throw error;
                }
                finally {
                    context.services.clientSecretQuery.release(
                        queryPromise,
                        context.config.linger,
                    );
                }
            },
        };
    };
}

import * as authApi from "@latency.gg/lgg-auth-oas";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createValidateRedirectUriOperation(
    context: application.Context,
): authApi.ValidateRedirectUriOperationHandler {
    return async function (incomingRequest) {
        const incomingEntity = await incomingRequest.entity();
        const clientIdBuffer = Buffer.from(incomingRequest.parameters.client, "base64");
        const redirectUri = new URL(incomingEntity.redirectUri);

        const valid = await withTransaction(
            context.services.pgPool,
            async pgClient => {
                const result = await pgClient.query(
                    `
select 1
from public.redirect_uri
where client_id = $1
and value = $2
;
`,
                    [
                        clientIdBuffer,
                        redirectUri.toString(),
                    ],
                );

                if (result.rowCount !== 1) {
                    return false;
                }

                return true;
            },
        );

        return {
            status: 200,
            parameters: {},
            entity() {
                return {
                    valid,
                };
            },
        };

    };
}

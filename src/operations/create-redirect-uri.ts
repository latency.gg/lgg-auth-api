import * as authApi from "@latency.gg/lgg-auth-oas";
import { withTransaction } from "table-access";
import * as application from "../application/index.js";

export function createCreateRedirectUriOperation(
    context: application.Context,
): authApi.CreateRedirectUriOperationHandler {
    return async function (incomingRequest) {
        const now = new Date();

        const incomingEntity = await incomingRequest.entity();
        const clientIdBuffer = Buffer.from(incomingRequest.parameters.client, "base64");
        const redirectUri = new URL(incomingEntity.value);

        await withTransaction(
            context.services.pgPool,
            async pgClient => {

                const result = await pgClient.query(
                    `
insert into public.redirect_uri (
    client_id, value, created_utc
)
values (
    $1, $2, $3
)
`,
                    [
                        clientIdBuffer,
                        redirectUri.toString(),
                        now.toISOString(),
                    ],
                );
            },
        );

        return {
            status: 201,
            parameters: {},
            entity() {
                return {};
            },
        };
    };
}

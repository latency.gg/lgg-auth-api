import * as prom from "prom-client";

export interface Config {
    endpoint: URL;

    pgUri: URL;

    accessTokenAge: number;
    refreshTokenAge: number;
    authorizationCodeAge: number;

    abortController: AbortController;
    promRegistry: prom.Registry;

    linger: number,
    retryIntervalBase: number,
    retryIntervalCap: number,

    connectionTimeout: number,
    connectionLinger: number,
    queryTimeout: number,
}

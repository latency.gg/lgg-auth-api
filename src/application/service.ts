import delay from "delay";
import pg from "pg";
import * as queries from "../queries/index.js";
import { Config } from "./config.js";

export interface Services {
    pgPool: pg.Pool;

    clientRowQuery: queries.ClientRowQuery,
    clientSecretRowQuery: queries.ClientSecretRowQuery,
    connectionRowQuery: queries.ConnectionRowQuery,
    clientOwnershipRowQuery: queries.ClientOwnershipRowQuery,

    clientQuery: queries.ClientQuery,
    clientSecretQuery: queries.ClientSecretQuery,
    clientOwnershipQuery: queries.ClientOwnershipQuery,
    connectionQuery: queries.ConnectionQuery,

    destroy: () => Promise<void>
}

export function createServices(
    config: Config,
    onError: (error: unknown) => void,
): Services {
    const pgPool = new pg.Pool({
        connectionString: config.pgUri.toString(),

        idleTimeoutMillis: config.connectionLinger,
        connectionTimeoutMillis: config.connectionTimeout,
        statement_timeout: 0,
        query_timeout: config.queryTimeout,
        idle_in_transaction_session_timeout: 0,
    });

    const clientRowQuery = queries.createClientRowQueryFactory(
        config,
        { pgPool },
        onError,
    );

    const clientSecretRowQuery = queries.createClientSecretRowQueryFactory(
        config,
        { pgPool },
        onError,
    );

    const clientOwnershipRowQuery = queries.createClientOwnershipRowQueryFactory(
        config,
        { pgPool },
        onError,
    );

    const connectionRowQuery = queries.createConnectionRowQueryFactory(
        config,
        { pgPool },
        onError,
    );

    const clientQuery = queries.createClientQueryFactory(
        config,
        { clientRowQuery },
        onError,
    );

    const clientSecretQuery = queries.createClientSecretQueryFactory(
        config,
        { clientSecretRowQuery },
        onError,
    );

    const clientOwnershipQuery = queries.createClientOwnershipQueryFactory(
        config,
        { clientOwnershipRowQuery },
        onError,
    );

    const connectionQuery = queries.createConnectionQueryFactory(
        config,
        { connectionRowQuery },
        onError,
    );

    const destroy = async () => {
        // flushing should happen after all of the leases are released!
        // we wait for a tick to make this happen
        await delay(0);
        //

        clientQuery.flush();
        clientSecretQuery.flush();
        clientOwnershipQuery.flush();
        connectionQuery.flush();

        clientRowQuery.flush();
        clientSecretRowQuery.flush();
        clientOwnershipRowQuery.flush();
        connectionRowQuery.flush();

        await pgPool.end();
    };

    return {
        pgPool,

        clientRowQuery,
        clientSecretRowQuery,
        clientOwnershipRowQuery,
        connectionRowQuery,

        clientQuery,
        clientSecretQuery,
        clientOwnershipQuery,
        connectionQuery,

        destroy,
    };
}
